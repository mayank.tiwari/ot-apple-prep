job "db" {
    datacenters = ["dc1"]
    
    group "db" {
        count = 1
        task "mongo" {
            driver = "docker"
            config {
                image = "mongo:latest"
            }
            
            env {
                MONGO_INITDB_ROOT_USERNAME="user"
                MONGO_INITDB_ROOT_PASSWORD="password"
            }
            resources {
                cpu = 500
                memory = 500
                network {
                    port "http" {
                        static = 27017
                    }
                }
            }
        }
        network {
            mode = "bridge"
        }
        service {
            name = "mongo"
            port = "27017"
            connect {
                sidecar_service {}
            }
        } 
    }
}
