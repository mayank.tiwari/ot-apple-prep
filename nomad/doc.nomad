job "webapplication" {
    datacenters = ["dc1"]
    update {
        max_parallel = 1
        stagger = "30s"
        healthy_deadline = "2m"

        canary = 1
        auto_revert = true
    }

    group "app" {
        count = 2
        task "app" {
            driver = "docker"
            config {
                image = "dockerhandson/java-web-app:latest"
                port_map {
                    http = 8080
                }
            }
            resources {
                cpu = 300
                memory = 350
                network {
                    port "http" {}
                }
            
            }
             
            service {
                name = "java-service"
            }
        }
    }
}
