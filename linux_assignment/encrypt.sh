#!/bin/bash


operation=$1
file=$2


#1. encrypt function

encrypt () {
if [[ -f "$2"]]; then
    gpg -c $2
fi
    rm $file
}

#2. decrypt function

decrypt () {
if [[ -f "$2"]]; then
    gpg $2
fi
}

case $operation in
    encrypt)
      encrypt
    ;;
    decrypt)
      decrypt
    ;;
    *)
    echo "Invalid argument!"
    ;;
esac
